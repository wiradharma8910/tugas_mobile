import 'package:flutter/material.dart';

void main() {
  runApp(MaterialApp(
      home: Scaffold(
          appBar: AppBar(
              title: Text('WiraDharmaApp'),
              backgroundColor: Colors.blueGrey,
              shadowColor: Colors.black,
              leading: IconButton(
                icon: Icon(Icons.web),
                onPressed: () {},
              ),
              actions: <Widget>[
                IconButton(
                  icon: Icon(
                    Icons.thumb_up,
                    color: Colors.white,
                  ),
                  onPressed: () {},
                ),
                IconButton(
                  icon: Icon(
                    Icons.thumb_down,
                    color: Colors.white,
                  ),
                  onPressed: () {},
                ),
              ]),
          body: Container(
              padding: EdgeInsets.all(50),
              child: Column(children: <Widget>[
                ClipRRect(
                  borderRadius: BorderRadius.circular(20),
                  child: Center(child: Image.asset('assets/jas.png')),
                ),
                Text('I Gede Wira Dharma Putra',
                    style: TextStyle(
                        color: Colors.blue,
                        fontSize: 20,
                        fontWeight: FontWeight.bold)),
              ])))));
}
